<?php 
return array (
  'site_name' => 'Verydows 开源电商系统',
  'home_title' => 'Verydows 开源电子商务系统 | 轻松开启电商之旅',
  'home_keywords' => 'Verydows, 电商系统, 商城系统, 开源商城, 开源电商, PHP电商, PHP商城',
  'home_description' => '',
  'footer_info' => '',
  'goods_search_per_num' => '20',
  'upload_filetype' => '.jpg|.jpeg|.gif|.png|.bmp|.swf|.flv|.avi|.rmvb',
  'upload_filesize' => '2MB',
  'captcha_admin_login' => '2',
  'captcha_user_login' => '2',
  'captcha_user_register' => '1',
  'captcha_feedback' => '1',
  'smtp_server' => '',
  'smtp_user' => '',
  'smtp_password' => '',
  'smtp_port' => '',
  'smtp_secure' => '',
  'admin_mult_ip_login' => '0',
  'upload_goods_filesize' => '300KB',
  'visitor_stats' => '1',
  'goods_hot_searches' => '',
  'cate_goods_per_num' => '20',
  'goods_history_num' => '5',
  'goods_related_num' => '5',
  'goods_review_per_num' => '10',
  'upload_goods_filetype' => '.jpg|.png|.gif',
  'show_goods_stock' => '0',
  'order_cancel_expires' => '5',
  'goods_img_thumb' => 
  array (
    0 => 
    array (
      'w' => 350,
      'h' => 350,
    ),
    1 => 
    array (
      'w' => 150,
      'h' => 150,
    ),
    2 => 
    array (
      'w' => 100,
      'h' => 100,
    ),
    3 => 
    array (
      'w' => 50,
      'h' => 50,
    ),
  ),
  'goods_album_thumb' => 
  array (
    0 => 
    array (
      'w' => 350,
      'h' => 350,
    ),
    1 => 
    array (
      'w' => 50,
      'h' => 50,
    ),
  ),
  'enabled_theme' => 'default',
  'user_consignee_limits' => '15',
  'upload_avatar_filesize' => '200KB',
  'order_delivery_expires' => '7',
  'user_register_email_verify' => '0',
  'user_review_approve' => '1',
  'rewrite_enable' => '1',
  'home_recommend_num' => '5',
  'home_newarrival_num' => '5',
  'home_bargain_num' => '5',
  'home_article_num' => '4',
  'data_cache_lifetime' => '7200',
  'goods_fulltext_query' => '0',
  'encrypt_key' => 'Jr%4rU=sB3KM55befw2=50NzIyeXS32S',
  'debug' => '1',
  'rewrite_rule' => 
  array (
    'pay/notify/<pcode>' => 'pay/notify',
    'pay/return/<pcode>' => 'pay/return',
    '404.html' => 'main/404',
    'search.html' => 'goods/search',
    'item/<id>.html' => 'goods/index',
    'cate/<id>.html' => 'category/index',
    'news/<id>.html' => 'article/index',
    'help/<id>.html' => 'help/index',
    '<a>/img' => 'image/<a>',
    'index.html' => 'main/index',
    '<c>/<a>.html' => '<c>/<a>',
  ),
  'http_host' => 'http://localhost/vds',
);