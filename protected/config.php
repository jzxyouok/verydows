<?php
date_default_timezone_set('PRC');

return array
(
    'mysql' => array
    (
        'MYSQL_HOST' => 'localhost',
        'MYSQL_PORT' => '3306',
        'MYSQL_USER' => 'root',
        'MYSQL_DB'   => 'vds160309',
		'MYSQL_PASS' => '123456',
        'MYSQL_DB_TABLE_PRE' => 'verydows_',
        'MYSQL_CHARSET' => 'UTF8',
    ),
    
    'verydows' => array
    (
        'VERSION' => '',
        'RELEASE' => '',
        'COMMENCED' => '1457523361',
    ),
);