![verydows logo](http://www.verydows.com/public/index/images/logo.gif)  
```
免费、开源、高效、安全的PHP电子商务系统
```


#### Verydows 特性简介

* 基于最新版超轻量级SpeedPHP框架编写的驱动核心，执行速度飞快
 
* 拥有完善的电商业务流程处理，能够满足绝大多数在线电商运营需求

* 强安全策略规则，多重数据过滤，重要信息加密传输，运行稳定安全

* 系统管理界面设计更注重用户体验，短时间内便能熟悉系统的全部操作

* 在线购物流程按照多个知名电商平台的下单体验设计，让用户在熟悉的流程中完成在线购买

* 采用图表化的数据统计，让数据对运营管理起到更加直观有效的参考和辅助

* 非常灵活的页面布局机制，前端页面中公用部分、广告部分完全按需调用

* 一站式程序安装，几分钟内就能建立起一套功能强大的在线商城


#### 运行环境

* 操作系统： Windows / Linux / Unix
 
* Web 服务器：Apache / Nginx / IIS

* PHP 版本：5.2 及以上

* MySQL 版本：5.0 及以上

* 生产环境推荐：Linux + Nginx + PHP5.5 + MySQL5.6


#### 相关链接

* 官网地址：http://www.verydows.com 

* 演示地址：http://demo.verydows.com

* 交流反馈：http://bbs.verydows.com


#### 联系我们

* 邮箱：besten#live.com (将“#”换成"@")

* QQ：150526401

* QQ讨论交流群：372701906